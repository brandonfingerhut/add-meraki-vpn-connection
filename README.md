Creates a Windows VPN Connection configured to connect to a Meraki Client VPN connection point.
