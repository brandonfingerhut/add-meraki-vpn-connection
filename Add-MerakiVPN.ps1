﻿<#
.DESCRIPTION
Creates a Windows VPN Connection configured to connect to a Meraki Client VPN connection point.

.NOTES
[Change Log]
2021-12-13 - 1.0.0
Release Candidate

#>

[CmdletBinding()]
Param (
	[Parameter(Mandatory=$true)]
    [string]$VPNProfile
)

# Import VPN Profiles XML
[xml]$VPNProfiles = Get-Content -Path ".\VPNProfiles.xml"

# Select requested VPN Profile from XML
$SelectedVPN = $VPNProfiles.VPNProfiles.Profile | Where-Object { $_.Name -eq "$VPNProfile" }

# If VPN already exists under former name, remove it
If(Get-VPNConnection -Name "$($SelectedVPN.FormerDisplayName)" -AllUserConnection -ErrorAction SilentlyContinue)
{
    Remove-VpnConnection -AllUserConnection -Name "$($SelectedVPN.FormerDisplayName)" -Force
}

# If VPN already exists under current name, remove it
If(Get-VPNConnection -Name "$($SelectedVPN.DisplayName)" -AllUserConnection -ErrorAction SilentlyContinue)
{
    Remove-VpnConnection -AllUserConnection -Name "$($SelectedVPN.DisplayName)" -Force
}

# Add VPN Connection
Add-VpnConnection -AllUserConnection -Name "$($SelectedVPN.DisplayName)" -ServerAddress "$($SelectedVPN.URL)"  -TunnelType L2tp -EncryptionLevel Optional -L2tpPsk "$($SelectedVPN.PSK)" -AuthenticationMethod Pap -Force

# Modify Configuration to require encryption
$FileName = "C:\ProgramData\Microsoft\Network\Connections\Pbk\rasphone.pbk"
$FileContents = Get-Content -path $FileName

$SearchString = "[" + "$($SelectedVPN.DisplayName)" + "]"
$i = 0

foreach($line in $FileContents) 
{
    if($line -eq $SearchString){
        $InCorrectVPNProfile = $true
    }
    if($InCorrectVPNProfile -and $line -eq "DataEncryption=8")
    {
        $FileContents[$i] = 'DataEncryption=256'
        $InCorrectVPNProfile = $false
    }
    $i++
}

$FileContents | Set-Content -Path $FileName